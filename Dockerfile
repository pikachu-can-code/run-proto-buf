FROM ubuntu:21.04

RUN mkdir -p /app
RUN apt update -y
RUN apt upgrade -y
RUN apt install wget -y
RUN wget https://golang.org/dl/go1.17.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.17.linux-amd64.tar.gz
RUN echo export PATH=$PATH:/usr/local/go/bin >> ~/.bash_profile
RUN source ~/.bash_profile
RUN go version
RUN go install google.golang.org/protobuf/cmd/protoc-gen-go
RUN go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
RUN go get -u github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway

RUN wget https://github.com/bufbuild/buf/releases/download/v0.54.1/buf-Linux-x86_64
RUN chmod +x buf-Linux-x86_64
RUN cp buf-Linux-x86_64 /usr/bin/buf

WORKDIR /app

ENTRYPOINT ["tail", "-f", "/dev/null"]
